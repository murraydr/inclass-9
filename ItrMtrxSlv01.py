# load library
import numpy as np
import time as tm

# give the entries in matrix A and column vector f
A = np.array([[-3, 1, 2],[-1, -3, 1],[-1, 3, 4]])  
nrm=0
f = np.array([2, 6, 4])
T=np.zeros(3)

# decompose A to D and R matrices
D=np.diag(np.diag(A))
R=A-D

## Test add a line to the file in branch.

# inverse of D matrix

Dinv = np.zeros((3,3))
# fill the entries in Dinv
Dinv=np.linalg.inv(D)
    

# initial guess value of x
x = np.array([0, 0, 0])

# walltime before matrix solving
cpu_tm_bg = tm.time()

# iteration
for iter in range(200):
    # use function np.matmul for matrix-vector multiplication
 
    # calculate T^(n+1) using matrix-vector operations
	T = np.matmul(Dinv,   (f-np.matmul(R,T)))
    
    

    # residual during iterations
	resd = f-np.matmul(A,T)


    
    # use calculate the norm of the residual using numpy function
	#nrm=np.linalg.norm(resd,2)
	nrm=0
	for i in range(3):		
		nrm+=resd[i]**2
	nrm=nrm**0.5

    

# walltime after matrix solving
cpu_tm_ed = tm.time()    

# print results
print(cpu_tm_ed - cpu_tm_bg, nrm)

